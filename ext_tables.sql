#
# Table structure for table 'tx_testexample_domain_model_car'
#
CREATE TABLE tx_testexample_domain_model_car (

	description varchar(255) DEFAULT '' NOT NULL,
	make int(11) unsigned DEFAULT '0' NOT NULL,
	model int(11) unsigned DEFAULT '0' NOT NULL,

);

#
# Table structure for table 'tx_testexample_domain_model_make'
#
CREATE TABLE tx_testexample_domain_model_make (

	title varchar(255) DEFAULT '' NOT NULL,
	models int(11) unsigned DEFAULT '0' NOT NULL,

);

#
# Table structure for table 'tx_testexample_domain_model_model'
#
CREATE TABLE tx_testexample_domain_model_model (

	title varchar(255) DEFAULT '' NOT NULL,

);

#
# Table structure for table 'tx_testexample_car_make_mm'
#
CREATE TABLE tx_testexample_car_make_mm (
	uid_local int(11) unsigned DEFAULT '0' NOT NULL,
	uid_foreign int(11) unsigned DEFAULT '0' NOT NULL,
	sorting int(11) unsigned DEFAULT '0' NOT NULL,
	sorting_foreign int(11) unsigned DEFAULT '0' NOT NULL,

	PRIMARY KEY (uid_local,uid_foreign),
	KEY uid_local (uid_local),
	KEY uid_foreign (uid_foreign)
);

#
# Table structure for table 'tx_testexample_car_model_mm'
#
CREATE TABLE tx_testexample_car_model_mm (
	uid_local int(11) unsigned DEFAULT '0' NOT NULL,
	uid_foreign int(11) unsigned DEFAULT '0' NOT NULL,
	sorting int(11) unsigned DEFAULT '0' NOT NULL,
	sorting_foreign int(11) unsigned DEFAULT '0' NOT NULL,

	PRIMARY KEY (uid_local,uid_foreign),
	KEY uid_local (uid_local),
	KEY uid_foreign (uid_foreign)
);

#
# Table structure for table 'tx_testexample_make_model_mm'
#
CREATE TABLE tx_testexample_make_model_mm (
	uid_local int(11) unsigned DEFAULT '0' NOT NULL,
	uid_foreign int(11) unsigned DEFAULT '0' NOT NULL,
	sorting int(11) unsigned DEFAULT '0' NOT NULL,
	sorting_foreign int(11) unsigned DEFAULT '0' NOT NULL,

	PRIMARY KEY (uid_local,uid_foreign),
	KEY uid_local (uid_local),
	KEY uid_foreign (uid_foreign)
);
