<?php
defined('TYPO3_MODE') or die();

//// This makes the plugin selectable in the BE.
//\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
//    // extension name, exactly matching the PHP namespaces (vendor and product)
//    'EXOTEC.TestExample',
//    // arbitrary, but unique plugin name (not visible in the BE)
//    'Pi1',
//    // plugin title, as visible in the drop-down in the BE
//    'LLL:test_example/Resources/Private/Language/locallang_db.xlf:tx_test_example_pi1.name',
//    // the icon visible in the drop-down in the BE
//    'EXT:test_example/Resources/Public/Icons/user_plugin_pi1.svg'
//);

// This removes the default controls from the plugin.
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_excludelist']['testexample_pi1'] = 'recursive,select_key,pages';
// These two commands add the flexform configuration for the plugin.
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist']['testexample_pi1'] = 'pi_flexform';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
    'testexample_pi1',
    'FILE:EXT:test_example/Configuration/FlexForms/Plugin.xml'
);
