<?php
declare(strict_types=1);

namespace EXOTEC\TestExample\Tests\Functional\Domain\Repository\Car;

use Nimut\TestingFramework\TestCase\FunctionalTestCase;
use EXOTEC\TestExample\Domain\Model\Car;
use EXOTEC\TestExample\Domain\Repository\CarRepository;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Object\ObjectManager;
use TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager;
use TYPO3\CMS\Extbase\Persistence\Generic\QueryResult;
use TYPO3\CMS\Extbase\Persistence\Generic\Typo3QuerySettings;
use TYPO3\CMS\Extbase\Persistence\ObjectStorage;

/**
 * Test case.
 *
 * @author Alexander Weber <weber@exotec.de>
 */
class CarRepositoryTest extends FunctionalTestCase
{
    /**
     * @var string[]
     */
    protected $testExtensionsToLoad = ['typo3conf/ext/test_example'];

    /**
     * @var CarRepository
     */
    private $subject = null;

    /**
     * @var Typo3QuerySettings
     */
    private $defaultQuerySettings = null;

    /**
     * @var PersistenceManager
     */
    private $persistenceManager = null;

    protected function setUp(): void
    {
        parent::setUp();

        $this->persistenceManager = GeneralUtility::makeInstance(PersistenceManager::class);

        /** @var ObjectManager $objectManager */
        $objectManager = GeneralUtility::makeInstance(ObjectManager::class);
        $this->subject = $objectManager->get(CarRepository::class);

        /** @var Typo3QuerySettings $this->defaultQuerySettings */
        $this->defaultQuerySettings = $objectManager->get(Typo3QuerySettings::class);
        $this->defaultQuerySettings->setRespectStoragePage(false);
    }

    /**
     * @test
     */
    public function find_all_without_storage_pid_returns_null(): void
    {
        $container = $this->subject->findAll();

        self::assertCount(0, $container);
    }

    /**
     * @test
     */
    public function find_all_get_all_records_from_storage_pid(): void
    {
        $this->importDataSet(__DIR__ . '/../Fixtures/Car/Car.xml');

        $this->subject->setDefaultQuerySettings($this->defaultQuerySettings);

        $container = $this->subject->findAll();
        self::assertInstanceOf(QueryResult::class, $container); // passed
        self::assertGreaterThanOrEqual(1, \count($container)); // Failed asserting that 0 is equal to 1 or is greater than 1.
        self::assertInstanceOf(Car::class, $container[0]); // Failed asserting that null is an instance of class "EXOTEC\TestExample\Domain\Model\Car"
    }


    /**
     * @test
     */
    public function find_by_uid_for_existimg_record_returns_model_with_data(): void
    {
        $this->importDataSet(__DIR__ . '/../Fixtures/Car/Car.xml');

        $uid = 3;
        /** @var Car $model */
        $car = $this->subject->findByUid($uid);

        self::assertNotNull($car);
        self::assertInstanceOf(Car::class, $car);
        self::assertSame('Sportback', $car->getDescription());
    }


    /**
     * @test
     */
    public function addAndPersistAllCreatesNewRecord(): void
    {
        $description = 'Godesberger Burgtee';
        $car = new Car();
        $car->setDescription($description);

        $this->subject->add($car);
        $this->persistenceManager->persistAll();

        $databaseRow = $this->getDatabaseConnection()->selectSingleRow(
            '*',
            'tx_testexample_domain_model_car',
            'uid = ' . $car->getUid()
        );
        self::assertSame($description, $databaseRow['description']);
    }

    /**
     * @test
     */
    public function updateAndPersistAllCreatesNewRecord(): void
    {
        $this->importDataSet(__DIR__ . '/../Fixtures/Car/Car.xml');

        $uid = 3;
        /** @var Car $model */
        $car = $this->subject->findByUid($uid);
        $car->setDescription('New description');

        $this->subject->update($car);
        $this->persistenceManager->persistAll();

        $databaseRow = $this->getDatabaseConnection()->selectSingleRow(
            '*',
            'tx_testexample_domain_model_car',
            'uid = ' . $car->getUid()
        );
        self::assertSame('New description', $databaseRow['description']);
    }
}
