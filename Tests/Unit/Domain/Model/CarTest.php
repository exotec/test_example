<?php
namespace EXOTEC\TestExample\Tests\Unit\Domain\Model;

/**
 * Test case.
 *
 * @author Alexander Weber <weber@exotec.de>
 */
class CarTest extends \TYPO3\TestingFramework\Core\Unit\UnitTestCase
{
    /**
     * @var \EXOTEC\TestExample\Domain\Model\Car
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \EXOTEC\TestExample\Domain\Model\Car();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getDescriptionReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getDescription()
        );
    }

    /**
     * @test
     */
    public function setDescriptionForStringSetsDescription()
    {
        $this->subject->setDescription('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'description',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getMakeReturnsInitialValueForMake()
    {
        $newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        self::assertEquals(
            $newObjectStorage,
            $this->subject->getMake()
        );
    }

    /**
     * @test
     */
    public function setMakeForObjectStorageContainingMakeSetsMake()
    {
        $make = new \EXOTEC\TestExample\Domain\Model\Make();
        $objectStorageHoldingExactlyOneMake = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $objectStorageHoldingExactlyOneMake->attach($make);
        $this->subject->setMake($objectStorageHoldingExactlyOneMake);

        self::assertAttributeEquals(
            $objectStorageHoldingExactlyOneMake,
            'make',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function addMakeToObjectStorageHoldingMake()
    {
        $make = new \EXOTEC\TestExample\Domain\Model\Make();
        $makeObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['attach'])
            ->disableOriginalConstructor()
            ->getMock();

        $makeObjectStorageMock->expects(self::once())->method('attach')->with(self::equalTo($make));
        $this->inject($this->subject, 'make', $makeObjectStorageMock);

        $this->subject->addMake($make);
    }

    /**
     * @test
     */
    public function removeMakeFromObjectStorageHoldingMake()
    {
        $make = new \EXOTEC\TestExample\Domain\Model\Make();
        $makeObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['detach'])
            ->disableOriginalConstructor()
            ->getMock();

        $makeObjectStorageMock->expects(self::once())->method('detach')->with(self::equalTo($make));
        $this->inject($this->subject, 'make', $makeObjectStorageMock);

        $this->subject->removeMake($make);
    }

    /**
     * @test
     */
    public function getModelReturnsInitialValueForModel()
    {
        $newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        self::assertEquals(
            $newObjectStorage,
            $this->subject->getModel()
        );
    }

    /**
     * @test
     */
    public function setModelForObjectStorageContainingModelSetsModel()
    {
        $model = new \EXOTEC\TestExample\Domain\Model\Model();
        $objectStorageHoldingExactlyOneModel = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $objectStorageHoldingExactlyOneModel->attach($model);
        $this->subject->setModel($objectStorageHoldingExactlyOneModel);

        self::assertAttributeEquals(
            $objectStorageHoldingExactlyOneModel,
            'model',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function addModelToObjectStorageHoldingModel()
    {
        $model = new \EXOTEC\TestExample\Domain\Model\Model();
        $modelObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['attach'])
            ->disableOriginalConstructor()
            ->getMock();

        $modelObjectStorageMock->expects(self::once())->method('attach')->with(self::equalTo($model));
        $this->inject($this->subject, 'model', $modelObjectStorageMock);

        $this->subject->addModel($model);
    }

    /**
     * @test
     */
    public function removeModelFromObjectStorageHoldingModel()
    {
        $model = new \EXOTEC\TestExample\Domain\Model\Model();
        $modelObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['detach'])
            ->disableOriginalConstructor()
            ->getMock();

        $modelObjectStorageMock->expects(self::once())->method('detach')->with(self::equalTo($model));
        $this->inject($this->subject, 'model', $modelObjectStorageMock);

        $this->subject->removeModel($model);
    }
}
