<?php
namespace EXOTEC\TestExample\Tests\Unit\Domain\Model;

use EXOTEC\TestExample\Domain\Model\Make;
use EXOTEC\TestExample\Domain\Model\Model;
use TYPO3\CMS\Extbase\Persistence\ObjectStorage;

/**
 * Test case.
 *
 * @author Alexander Weber <weber@exotec.de>
 */
class FilterTest extends \TYPO3\TestingFramework\Core\Unit\UnitTestCase
{
    /**
     * @var \EXOTEC\TestExample\Domain\Model\Filter
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \EXOTEC\TestExample\Domain\Model\Filter();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getDescriptionReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getDescription()
        );
    }

    /**
     * @test
     */
    public function setDescriptionForStringSetsDescription()
    {
        $this->subject->setDescription('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'description',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getMakeReturnsInitialValueForMake()
    {
        $objectStorage = new ObjectStorage();
        self::assertEquals(
            $objectStorage,
            $this->subject->getMake()
        );
    }

    /**
     * @test
     */
    public function setMakeForMakeSetsMake()
    {
        $makeFixture = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $this->subject->setMake($makeFixture);

        self::assertAttributeEquals(
            $makeFixture,
            'make',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getModelReturnsInitialValueForModel()
    {
        $objectStorage =  new ObjectStorage();
        self::assertEquals(
            $objectStorage,
            $this->subject->getModel()
        );
    }

    /**
     * @test
     */
    public function setModelForModelSetsModel()
    {
        $modelFixture = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $this->subject->setModel($modelFixture);

        self::assertAttributeEquals(
            $modelFixture,
            'model',
            $this->subject
        );
    }
}
