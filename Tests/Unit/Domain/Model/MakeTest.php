<?php
namespace EXOTEC\TestExample\Tests\Unit\Domain\Model;

/**
 * Test case.
 *
 * @author Alexander Weber <weber@exotec.de>
 */
class MakeTest extends \TYPO3\TestingFramework\Core\Unit\UnitTestCase
{
    /**
     * @var \EXOTEC\TestExample\Domain\Model\Make
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \EXOTEC\TestExample\Domain\Model\Make();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getTitleReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getTitle()
        );
    }

    /**
     * @test
     */
    public function setTitleForStringSetsTitle()
    {
        $this->subject->setTitle('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'title',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getModelsReturnsInitialValueForModel()
    {
        $newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        self::assertEquals(
            $newObjectStorage,
            $this->subject->getModels()
        );
    }

    /**
     * @test
     */
    public function setModelsForObjectStorageContainingModelSetsModels()
    {
        $model = new \EXOTEC\TestExample\Domain\Model\Model();
        $objectStorageHoldingExactlyOneModels = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $objectStorageHoldingExactlyOneModels->attach($model);
        $this->subject->setModels($objectStorageHoldingExactlyOneModels);

        self::assertAttributeEquals(
            $objectStorageHoldingExactlyOneModels,
            'models',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function addModelToObjectStorageHoldingModels()
    {
        $model = new \EXOTEC\TestExample\Domain\Model\Model();
        $modelsObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['attach'])
            ->disableOriginalConstructor()
            ->getMock();

        $modelsObjectStorageMock->expects(self::once())->method('attach')->with(self::equalTo($model));
        $this->inject($this->subject, 'models', $modelsObjectStorageMock);

        $this->subject->addModel($model);
    }

    /**
     * @test
     */
    public function removeModelFromObjectStorageHoldingModels()
    {
        $model = new \EXOTEC\TestExample\Domain\Model\Model();
        $modelsObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['detach'])
            ->disableOriginalConstructor()
            ->getMock();

        $modelsObjectStorageMock->expects(self::once())->method('detach')->with(self::equalTo($model));
        $this->inject($this->subject, 'models', $modelsObjectStorageMock);

        $this->subject->removeModel($model);
    }
}
