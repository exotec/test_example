<?php
declare(strict_types=1);

namespace EXOTEC\TestExample\Tests\Acceptance;

/**
 * Test case.
 *
 * @author Oliver Klee <typo3-coding@oliverklee.de>
 */
class StarterCest
{
    public function _before(\AcceptanceTester $I): void
    {
        $I->amOnPage('/');
    }

//    public function seeAuthorName(\AcceptanceTester $I): void
//    {
//        $I->see('Alexander Weber');
//    }

    public function fillMailFormAndSend(\AcceptanceTester $I): void
    {
        $I->fillField('#powermail_field_ihrname', 'Acceptance Tester');
        $I->fillField('#powermail_field_ihreemail', 'tester@exotec.de');
        $I->fillField('#powermail_field_ihrenachricht', 'Hello world');
        $I->click('.powermail_submit');
        $I->see('Vielen Dank für Ihre Nachricht');
    }

    public function canNavigateToImprint(\AcceptanceTester $I): void
    {
        $I->click('Impressum');
        $I->amOnPage('/impressum');
        $I->see('Impressum');
    }

    public function canNavigateToImprintAndThenToPrivacyStatement(\AcceptanceTester $I): void
    {
        $I->click('Impressum');
        $I->click('Datenschutz');
        $I->amOnPage('/footer/datenschutz');
        $I->see('Datenschutzerklärung');
    }
}
