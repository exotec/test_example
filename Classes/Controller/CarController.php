<?php
namespace EXOTEC\TestExample\Controller;

use EXOTEC\TestExample\Domain\Model\Car;
use EXOTEC\TestExample\Domain\Model\Filter;
use EXOTEC\TestExample\Domain\Model\Make;
use EXOTEC\TestExample\Domain\Model\Model;
use EXOTEC\TestExample\Domain\Repository\CarRepository;
use EXOTEC\TestExample\Domain\Repository\MakeRepository;
use EXOTEC\TestExample\Domain\Repository\ModelRepository;
use stdClass;
use TYPO3\CMS\Extbase\Persistence\ObjectStorage;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;
use TYPO3\CMS\Extbase\Annotation\IgnoreValidation;

/***
 *
 * This file is part of the "Test example" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2019 Alexander Weber <weber@exotec.de>, exotec
 *
 ***/
/**
 * CarController
 */
class CarController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{

    /**
     * @var carRepository
     */
    protected $carRepository = null;

    public function injectCarRepository(CarRepository $carRepository): void
    {
        $this->carRepository = $carRepository;
    }

    /**
     * @var makeRepository
     */
    protected $makeRepository = null;

    public function injectMakeRepository(MakeRepository $makeRepository): void
    {
        $this->makeRepository = $makeRepository;
    }

    /**
     * @var modelRepository
     */
    protected $modelRepository = null;

    public function injectModelRepository(ModelRepository $modelRepository): void
    {
        $this->modelRepository = $modelRepository;
    }

    /**
     * action list
     *
     * @param \EXOTEC\TestExample\Domain\Model\Filter $filter
     * @IgnoreValidation("filter")
     * @return void
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\InvalidQueryException
     */
    public function listAction($filter = '')
    {
        $cars = $this->carRepository->findAll();
        $makes = $this->makeRepository->findAll();
        $makesOptions = $this->getSelectOptionItems($makes, 1);
        $modelsOptions= array();

        if (!$filter) {
            $filter = new Filter();

        } else {
            $cars = $this->carRepository->findFiltered($filter);

            if( $filter->getMake()->current() instanceof Make) {
                $modelsOptions = $this->getSelectOptionItems($filter->getMake()->current()->getModels(), 1);
            }
        }

        $values = array(
            'cars' => $cars, 'filter' => $filter, 'makes' => $makesOptions, 'models' => $modelsOptions,
        );

        $this->view->assignMultiple($values);
    }


    /**
     * @param ObjectStorage $entries
     * @param boolean $counter
     * @return array
     */
    public function getSelectOptionItems($entries, $counter=false)
    {
        $items = array();
        foreach ($entries as $entry) {
            $item = new stdClass();
            $item->key = $entry->getUid();
            $item->value = $entry->getTitle();
            if( $counter == true) {
                $number = $this->carRepository->selectOptionsCounter($entry);
                $item->value = $entry->getTitle().' ('.$number.')';
                if($number == 0) {
                    $item->disabled = 1;
                }
            }
            $items[] = $item;
        }
        return $items;
    }

    /**
     * action show
     * 
     * @param Car $car
     * @return void
     */
    public function showAction(Car $car)
    {
        $this->view->assign('car', $car);
    }

    /**
     * action new
     * 
     * @return void
     */
    public function newAction()
    {
        $makes = $this->makeRepository->findAll();
        $makesOptions = $this->getSelectOptionItems($makes);
        $models = $this->modelRepository->findAll();
        $modelsOptions = $this->getSelectOptionItems($models);

        $values = array(
            'makes' => $makesOptions,
            'models' => $modelsOptions
        );
        $this->view->assignMultiple($values);
    }


    /**
     * @param Car $newCar
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\StopActionException
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\UnsupportedRequestTypeException
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\IllegalObjectTypeException
     */
    public function createAction(Car $newCar)
    {
        $this->addFlashMessage('The object was created. Please be aware that this action is publicly accessible unless you implement an access check. See https://docs.typo3.org/typo3cms/extensions/extension_builder/User/Index.html', '', \TYPO3\CMS\Core\Messaging\AbstractMessage::WARNING);
        $this->carRepository->add($newCar);
        $this->redirect('list','Car','TestExample',[], $this->settings['listPid']);
    }

    /**
     * action edit
     * 
     * @param Car $car
     * @IgnoreValidation("car")
     * @return void
     */
    public function editAction(Car $car)
    {
        $makes = $this->makeRepository->findAll();
        $makesOptions = $this->getSelectOptionItems($makes);
        $models = $this->modelRepository->findAll();
        $modelsOptions = $this->getSelectOptionItems($models);

        $values['car'] = $car;
        $values = array(
            'car' => $car,
            'makes' => $makesOptions,
            'models' => $modelsOptions
        );
        $this->view->assignMultiple($values);
    }


    /**
     * @param Car $car
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\StopActionException
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\UnsupportedRequestTypeException
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\IllegalObjectTypeException
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\UnknownObjectException
     */
    public function updateAction(Car $car)
    {
        $this->addFlashMessage('The object was updated. Please be aware that this action is publicly accessible unless you implement an access check. See https://docs.typo3.org/typo3cms/extensions/extension_builder/User/Index.html', '', \TYPO3\CMS\Core\Messaging\AbstractMessage::WARNING);
        $this->carRepository->update($car);
        $this->redirect('edit', 'Car','TestExample', array('car' => $car->getUid()));
    }


    /**
     * @param Car $car
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\StopActionException
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\UnsupportedRequestTypeException
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\IllegalObjectTypeException
     */
    public function deleteAction(Car $car)
    {
        $this->addFlashMessage('The object was deleted. Please be aware that this action is publicly accessible unless you implement an access check. See https://docs.typo3.org/typo3cms/extensions/extension_builder/User/Index.html', '', \TYPO3\CMS\Core\Messaging\AbstractMessage::WARNING);
        $this->carRepository->remove($car);
        $this->redirect('list');
    }


    /**
     * @param $filter
     * @param $modelsAsArray
     * @return mixed
     */
    public function getFilteredModels($filter, $modelsAsArray)
    {
        $filteredModels = $filter->getMake()->getModels()->toArray();
        /** @var Model $filteredModel */
        foreach ($filteredModels as $index => $filteredModel) {
            $modelsAsArray[$index]['uid'] = $filteredModel->getUid();
            $modelsAsArray[$index]['title'] = $filteredModel->getTitle();
        }
        return $modelsAsArray;
    }

}
