<?php
namespace EXOTEC\TestExample\Domain\Model;


/***
 *
 * This file is part of the "Test example" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2019 Alexander Weber <weber@exotec.de>, exotec
 *
 ***/
/**
 * Model
 */
class Model extends \TYPO3\CMS\Extbase\DomainObject\AbstractValueObject
{

    /**
     * title
     * 
     * @var string
     */
    protected $title = '';

    /**
     * Returns the title
     * 
     * @return string $title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Sets the title
     * 
     * @param string $title
     * @return void
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }
}
