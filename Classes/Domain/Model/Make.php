<?php
namespace EXOTEC\TestExample\Domain\Model;


/***
 *
 * This file is part of the "Test example" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2019 Alexander Weber <weber@exotec.de>, exotec
 *
 ***/
/**
 * Make
 */
class Make extends \TYPO3\CMS\Extbase\DomainObject\AbstractValueObject
{

    /**
     * title
     * 
     * @var string
     */
    protected $title = '';

    /**
     * models
     * 
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\EXOTEC\TestExample\Domain\Model\Model>
     * @TYPO3\CMS\Extbase\Annotation\ORM\Cascade("remove")
     */
    protected $models = null;

    /**
     * Returns the title
     * 
     * @return string $title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Sets the title
     * 
     * @param string $title
     * @return void
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * __construct
     */
    public function __construct()
    {

        //Do not remove the next line: It would break the functionality
        $this->initStorageObjects();
    }

    /**
     * Initializes all ObjectStorage properties
     * Do not modify this method!
     * It will be rewritten on each save in the extension builder
     * You may modify the constructor of this class instead
     * 
     * @return void
     */
    protected function initStorageObjects()
    {
        $this->models = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
    }

    /**
     * Adds a Model
     * 
     * @param \EXOTEC\TestExample\Domain\Model\Model $model
     * @return void
     */
    public function addModel(\EXOTEC\TestExample\Domain\Model\Model $model)
    {
        $this->models->attach($model);
    }

    /**
     * Removes a Model
     * 
     * @param \EXOTEC\TestExample\Domain\Model\Model $modelToRemove The Model to be removed
     * @return void
     */
    public function removeModel(\EXOTEC\TestExample\Domain\Model\Model $modelToRemove)
    {
        $this->models->detach($modelToRemove);
    }

    /**
     * Returns the models
     * 
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\EXOTEC\TestExample\Domain\Model\Model> $models
     */
    public function getModels()
    {
        return $this->models;
    }

    /**
     * Sets the models
     * 
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\EXOTEC\TestExample\Domain\Model\Model> $models
     * @return void
     */
    public function setModels(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $models)
    {
        $this->models = $models;
    }

    /**
     * @param int $uid
     */
    public function setUid(int $uid): void
    {
        $this->uid = $uid;
    }

}
