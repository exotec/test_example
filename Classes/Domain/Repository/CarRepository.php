<?php
namespace EXOTEC\TestExample\Domain\Repository;

use EXOTEC\TestExample\Domain\Model\Make;
use EXOTEC\TestExample\Domain\Model\Model;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

/***
 *
 * This file is part of the "Test example" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2019 Alexander Weber <weber@exotec.de>, exotec
 *
 ***/
/**
 * The repository for Cars
 */
class CarRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{

    /**
     * @var array
     */
    protected $defaultOrderings = ['sorting' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING];

    /**
     * @param \EXOTEC\TestExample\Domain\Model\Filter $filter
     * @return array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\InvalidQueryException
     */
    public function findFiltered(\EXOTEC\TestExample\Domain\Model\Filter $filter)
    {
        $query = $this->createQuery();
        $constraints = [];
        // uid > 0 constraint to get all entries if filter empty
        $constraints[] = $query->greaterThan('uid', 0);
        if ($filter->getDescription()) {
            $constraints[] = $query->like('description', '%'.$filter->getDescription().'%');
        }
        if ( $filter->getMake()->current() instanceof Make) {
            $constraints[] = $query->contains('make', $filter->getMake());
        }
        if ($filter->getModel()->current() instanceof Model) {
            $constraints[] = $query->contains('model', $filter->getModel());
        }

        $query->matching($query->logicalAnd($constraints));
        return $query->execute();
    }

    public function selectOptionsCounter($entry)
    {
        $query = $this->createQuery();

        switch ( get_class($entry) ) {
            case Make::class :
                $constraints[] = $query->contains('make', $entry);
                break;

            case Model::class :
                $constraints[] = $query->contains('model', $entry);
                break;
        }

        $query->matching($query->logicalAnd($constraints));
        return $query->execute()->count();
    }
}
