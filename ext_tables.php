<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
            'EXOTEC.TestExample',
            'Pi1',
            'Example Plugin',
            'EXT:test_example/Resources/Public/Icons/user_plugin_pi1.svg'
        );

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('test_example', 'Configuration/TypoScript', 'Test example');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_testexample_domain_model_car', 'EXT:test_example/Resources/Private/Language/locallang_csh_tx_testexample_domain_model_car.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_testexample_domain_model_car');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_testexample_domain_model_make', 'EXT:test_example/Resources/Private/Language/locallang_csh_tx_testexample_domain_model_make.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_testexample_domain_model_make');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_testexample_domain_model_model', 'EXT:test_example/Resources/Private/Language/locallang_csh_tx_testexample_domain_model_model.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_testexample_domain_model_model');

    }
);
